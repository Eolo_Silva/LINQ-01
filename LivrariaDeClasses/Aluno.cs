﻿namespace LivrariaDeClasses
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    public class Aluno
    {
        //esta classe é criada onde leva as propiedades do aluno
        public string PrimeiroNome { get; set; }
        public string Apelido { get; set; }
        public DateTime DataNascimento {get; set;}
        public int DisciplinasFeitas { get; set; }
        public string NomeCompleto => $"{PrimeiroNome} {Apelido}";
    }
}
