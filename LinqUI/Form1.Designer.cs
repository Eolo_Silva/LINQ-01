﻿namespace LinqUI
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.ComboBoxTodosAlunos = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.numericUpDownDisciplinasFeitas = new System.Windows.Forms.NumericUpDown();
            this.ButtonUpdate = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.ListBoxFiltro = new System.Windows.Forms.ListBox();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownDisciplinasFeitas)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(-2, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(152, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "Todos os Alunos :";
            // 
            // ComboBoxTodosAlunos
            // 
            this.ComboBoxTodosAlunos.FormattingEnabled = true;
            this.ComboBoxTodosAlunos.Location = new System.Drawing.Point(2, 53);
            this.ComboBoxTodosAlunos.Name = "ComboBoxTodosAlunos";
            this.ComboBoxTodosAlunos.Size = new System.Drawing.Size(198, 21);
            this.ComboBoxTodosAlunos.TabIndex = 2;
            this.ComboBoxTodosAlunos.SelectedIndexChanged += new System.EventHandler(this.ComboBoxTodosAlunos_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(-2, 165);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(166, 20);
            this.label2.TabIndex = 3;
            this.label2.Text = "Desciplinas Feitas :";
            // 
            // numericUpDownDisciplinasFeitas
            // 
            this.numericUpDownDisciplinasFeitas.Location = new System.Drawing.Point(170, 165);
            this.numericUpDownDisciplinasFeitas.Name = "numericUpDownDisciplinasFeitas";
            this.numericUpDownDisciplinasFeitas.Size = new System.Drawing.Size(60, 20);
            this.numericUpDownDisciplinasFeitas.TabIndex = 4;
            // 
            // ButtonUpdate
            // 
            this.ButtonUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonUpdate.Location = new System.Drawing.Point(154, 307);
            this.ButtonUpdate.Name = "ButtonUpdate";
            this.ButtonUpdate.Size = new System.Drawing.Size(75, 23);
            this.ButtonUpdate.TabIndex = 5;
            this.ButtonUpdate.Text = "UPDATE";
            this.ButtonUpdate.UseVisualStyleBackColor = true;
            this.ButtonUpdate.Click += new System.EventHandler(this.ButtonUpdate_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(231, 30);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(149, 20);
            this.label3.TabIndex = 6;
            this.label3.Text = "Alunos Filtrados :";
            // 
            // ListBoxFiltro
            // 
            this.ListBoxFiltro.FormattingEnabled = true;
            this.ListBoxFiltro.Location = new System.Drawing.Point(235, 53);
            this.ListBoxFiltro.Name = "ListBoxFiltro";
            this.ListBoxFiltro.Size = new System.Drawing.Size(194, 277);
            this.ListBoxFiltro.TabIndex = 7;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(441, 350);
            this.Controls.Add(this.ListBoxFiltro);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.ButtonUpdate);
            this.Controls.Add(this.numericUpDownDisciplinasFeitas);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.ComboBoxTodosAlunos);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form LINQ";
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownDisciplinasFeitas)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox ComboBoxTodosAlunos;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown numericUpDownDisciplinasFeitas;
        private System.Windows.Forms.Button ButtonUpdate;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListBox ListBoxFiltro;
    }
}

