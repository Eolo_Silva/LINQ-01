﻿namespace LinqUI
{
    using LivrariaDeClasses;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Windows.Forms;
    public partial class Form1 : Form
    {
        List<Aluno> Alunos = ListaDeAlunos.LoadAlunos();

        public Form1()
        {
            InitializeComponent();
            InitCombo();
        }
        private void InitCombo()
        {
            ComboBoxTodosAlunos.DataSource = Alunos;
            ComboBoxTodosAlunos.DisplayMember = "NomeCompleto";//para carregar na combo nome completo

            ListBoxFiltro.DataSource = Alunos.Where(x => x.DisciplinasFeitas > 10).
                OrderBy(x => x.PrimeiroNome).
                ThenBy(x => x.Apelido).ToList(); // ordena menos que 10 e o mes igual a 3
            ListBoxFiltro.DisplayMember = "NomeCompleto";

        }

        private void ComboBoxTodosAlunos_SelectedIndexChanged(object sender, EventArgs e)
        {
            Aluno alunoSelecionado = (Aluno)ComboBoxTodosAlunos.SelectedItem; //aluno entre parentesis diz ao combobox selecionado o quem tem que fazer "CAST"  
                                                                              // se tiver que por alguma verificação utilizar por ex if (alunoSelecionado == null) ou try catch

            numericUpDownDisciplinasFeitas.Value = alunoSelecionado.DisciplinasFeitas; //recebe as disciplinas feitas pelo aluno da combobox


        }

        private void ButtonUpdate_Click(object sender, EventArgs e)
        {
            Aluno alunoSelecionado = (Aluno)ComboBoxTodosAlunos.SelectedItem;

            alunoSelecionado.DisciplinasFeitas = Convert.ToInt32(numericUpDownDisciplinasFeitas.Value);

            UpdateData();

        }
        private void UpdateData()
        {
            ListBoxFiltro.DataSource = Alunos.Where(x => x.DisciplinasFeitas > 10).
                OrderBy(x => x.PrimeiroNome).
                ThenBy(x => x.Apelido).ToList(); // ordena menos que 10 e o mes igual a 3
        }
    }
}
