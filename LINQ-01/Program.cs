﻿namespace LINQ_01
{
    using LivrariaDeClasses;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    class Program
    {
        static void Main(string[] args)
        {
            List<Aluno> Alunos = ListaDeAlunos.LoadAlunos();

            //usando o linq
            //ordenar por apelido
            //Alunos = Alunos.OrderBy(x => x.Apelido).ToList(); //ordena crescente
            //Alunos = Alunos.OrderByDescending(x => x.Apelido).ToList(); //ordena decrescente
            //Alunos = Alunos.OrderByDescending(x => x.Apelido).ThenByDescending(x => x.DisciplinasFeitas).ToList(); //ordena e se tiver varios iguais ainda ordena por outro criterio
            //Alunos = Alunos.Where(x => x.DisciplinasFeitas > 10 && x.DataNascimento.Month == 3).ToList(); // ordena menos que 10 e o mes igual a 3

            //foreach (var aluno in Alunos) //para cada uma das alinhas anteriores deverá criar-se um foreach
            //{
            //    Console.WriteLine($"{aluno.PrimeiroNome} {aluno.Apelido} {aluno.DataNascimento.ToShortDateString()} Disciplinas Feitas: {aluno.DisciplinasFeitas}");
            //}

            int totalDisciplinasFeitas = Alunos.Sum(x => x.DisciplinasFeitas);
            double mediaDisciplinasFeitas = Alunos.Average(x => x.DisciplinasFeitas);

            Console.WriteLine($"Total de disciplinas feitas: {totalDisciplinasFeitas}");
            Console.WriteLine($"Media de disciplinas feitas: {mediaDisciplinasFeitas:N2}");

            Console.ReadKey();

        }
    }
}
